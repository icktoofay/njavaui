#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <limits.h>
#include <dlfcn.h>
#include <jni.h>

static char *find_java(void) {
	const char *path = getenv("PATH");
	while(path) {
		const char *end = strchr(path, ':');
		if(end == NULL) {
			end = path + strlen(path);
		}
		size_t length = end - path;
		char *buffer = malloc(length + 6);
		if(buffer == NULL) {
			return NULL;
		}
		memcpy(buffer, path, length);
		memcpy(buffer + length, "/java", 6);
		struct stat statbuf;
		if(stat(buffer, &statbuf) != -1) {
			return buffer;
		}
		free(buffer);
		path = end;
		if(*path == '\0') {
			break;
		}
		++path;
	}
	return NULL;
}

static char *alloc_readlink(const char *link) {
	size_t buf_size = 16;
	for(;;) {
		char *buffer = malloc(buf_size);
		if(!buffer) {
			return NULL;
		}
		ssize_t filled = readlink(link, buffer, buf_size);
		if(filled == -1) {
			free(buffer);
			return NULL;
		}
		if(filled < buf_size) {
			buffer[filled] = '\0';
			return buffer;
		}
		free(buffer);
		if(buf_size >= ((size_t)-1) / 2) {
			return NULL;
		}
		buf_size *= 2;
	}
}

static char *recursive_alloc_readlink(const char *link) {
	char *last = strdup(link);
	if(last == NULL) {
		return NULL;
	}
	for(;;) {
		char *resolved = alloc_readlink(last);
		if(resolved == NULL) {
			return last;
		}
		free(last);
		last = resolved;
	}
}

static char *java_bin_to_lib(const char *java_bin) {
	const char *last_slash = strrchr(java_bin, '/');
	if(last_slash == NULL) {
		return strdup("/../lib/amd64/server/libjvm.so" + 1);
	}
	size_t dirname_length = last_slash - java_bin;
	char *lib_file = malloc(dirname_length + 31);
	if(lib_file != NULL) {
		memcpy(lib_file, java_bin, dirname_length);
		memcpy(lib_file + dirname_length,
			"/../lib/amd64/server/libjvm.so", 31);
	}
	return lib_file;
}

struct java_lib {
	jint (*GetDefaultJavaVMInitArgs)(void *vm_args);
	jint (*CreateJavaVM)(JavaVM **vm, JNIEnv **env, void *vm_args);
};

static void *open_java_lib(struct java_lib *fptab) {
	char *java_lib_file = NULL;
	char *java_loc = find_java();
	if(java_loc == NULL) {
		return NULL;
	}
	char *resolved_java_loc = recursive_alloc_readlink(java_loc);
	free(java_loc);
	if(resolved_java_loc == NULL) {
		return NULL;
	}
	java_lib_file = java_bin_to_lib(resolved_java_loc);
	free(resolved_java_loc);
	if(java_lib_file == NULL) {
		return NULL;
	}
	void *handle = dlopen(java_lib_file, RTLD_LAZY);
	free(java_lib_file);
	if(handle != NULL) {
		fptab->GetDefaultJavaVMInitArgs =
			dlsym(handle, "JNI_GetDefaultJavaVMInitArgs");
		if(fptab->GetDefaultJavaVMInitArgs == NULL) {
			dlclose(handle);
			return NULL;
		}
		fptab->CreateJavaVM = dlsym(handle, "JNI_CreateJavaVM");
		if(fptab->CreateJavaVM == NULL) {
			dlclose(handle);
			return NULL;
		}
	}
	return handle;
}

int main(int argc, char **argv) {
	struct java_lib java_lib;
	if(open_java_lib(&java_lib) == NULL) {
		fprintf(stderr, "You must have Java installed\n");
		return EXIT_FAILURE;
	}
	JavaVMInitArgs vm_args;
	memset(&vm_args, 0, sizeof(vm_args));
	vm_args.version = JNI_VERSION_1_6;
	if(java_lib.GetDefaultJavaVMInitArgs(&vm_args) != JNI_OK) {
		fprintf(stderr, "Your Java is too old\n");
		return EXIT_FAILURE;
	}
	JavaVM *java_vm;
	JNIEnv *env;
	if(java_lib.CreateJavaVM(&java_vm, &env, &vm_args) != JNI_OK) {
		fprintf(stderr, "Java initialization error\n");
		return EXIT_FAILURE;
	}
	jclass frame_class = (*env)->FindClass(env, "javax/swing/JFrame");
	if(frame_class == NULL) {
		goto java_error;
	}
	jmethodID frame_ctor = (*env)->GetMethodID(
		env, frame_class, "<init>", "()V");
	if(frame_ctor == NULL) {
		goto java_error;
	}
	jmethodID frame_set_title = (*env)->GetMethodID(
		env, frame_class, "setTitle", "(Ljava/lang/String;)V");
	if(frame_set_title == NULL) {
		goto java_error;
	}
	jmethodID frame_set_default_close_operation = (*env)->GetMethodID(
		env, frame_class, "setDefaultCloseOperation", "(I)V");
	if(frame_set_default_close_operation == NULL) {
		goto java_error;
	}
	jmethodID frame_set_size = (*env)->GetMethodID(
		env, frame_class, "setSize", "(II)V");
	if(frame_set_size == NULL) {
		goto java_error;
	}
	jmethodID frame_set_visible = (*env)->GetMethodID(
		env, frame_class, "setVisible", "(Z)V");
	if(frame_set_visible == NULL) {
		goto java_error;
	}
	jobject frame = (*env)->NewObject(env, frame_class, frame_ctor);
	if(frame == NULL) {
		goto java_error;
	}
	jstring title = (*env)->NewStringUTF(env, "Hello, world!");
	if(title == NULL) {
		goto java_error;
	}
	(*env)->CallVoidMethod(env, frame, frame_set_title, title);
	if((*env)->ExceptionOccurred(env)) {
		goto java_error;
	}
	(*env)->CallVoidMethod(
		env, frame, frame_set_default_close_operation, 3);
	if((*env)->ExceptionOccurred(env)) {
		goto java_error;
	}
	(*env)->CallVoidMethod(env, frame, frame_set_size, 300, 200);
	if((*env)->ExceptionOccurred(env)) {
		goto java_error;
	}
	(*env)->CallVoidMethod(env, frame, frame_set_visible, 1);
	if((*env)->ExceptionOccurred(env)) {
		goto java_error;
	}
	for(;;) {
		sleep(86400);
	}
	return EXIT_SUCCESS;

java_error:
	(*env)->ExceptionDescribe(env);
	return EXIT_FAILURE;
}
