JNI_CFLAGS = \
	-I$(shell dirname $$(readlink -e $$(which java)))/../../include \
	-I$(shell dirname $$(readlink -e $$(which java)))/../../include/linux

njavaui: njavaui.c
	gcc $(JNI_CFLAGS) -s -Os -o $@ $< -ldl
